Simple script for photographers to make collages for test prints.

Currently input images can be
- resized, in which case they must have the same ratio as the box prepared for them,
- cropped to fit the box,
- rotated by 90 deg clockwise.

You can see the overall look of the photograph, or you can see how selected details will look in the final print.

Currently only 3x16 bit images are supported (tested with 16 bit PNG).

Example file

```yaml
collage.png:
  size_px:
    width: 6000
    height: 6000
  collage:
  - - input: file1.png
      ops:
      - resize:
    - input: file1.png
      ops:
      - crop:
  - - input: file2.png
      ops:
      - resize:
    - input: file2.png
      ops:
      - rotate:
      - crop:
          x: 2600
          y: 0
  - - input: file3.png
      ops:
      - resize:
    - input: file3.png
      ops:
      - crop:
```

In this example collage1.png is made out of three sections, each section consists of another two sections.
The smaller sections are the same source image, one scaled to size, the other is cropped.
Cropping is done starting from top left corner to the size, except file2.png, which is cropped from position (2600,0)