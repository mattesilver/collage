if __name__ == '__main__':
    import logging
    from pathlib import Path

    import ruamel.yaml

    from collage.collage import Script


    logging.basicConfig()
    logging.getLogger('collage').setLevel(logging.DEBUG)

    text = Path('collage.yaml').read_text()
    yaml = ruamel.yaml.YAML(typ='safe')
    doc = yaml.load(text)
    script = Script.parse_obj(doc)
    script(Path())
