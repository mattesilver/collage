import logging
import math
from typing import Literal

import cv2
import numpy as np

from .base import Operator
from .types import Box, Point, Size

logger = logging.getLogger(__name__)


class CropOp(Operator):
    x: int = 0
    y: int = 0

    def __call__(self, img: np.ndarray, size: Size) -> np.ndarray:
        logger.debug('crop %s', Box.combine(Point(self.x, self.y), size))
        return img[
              self.y:self.y + size.width,
              self.x:self.x + size.height
              ]

    def __str__(self):
        return 'crop'


class ResizeOp(Operator):
    auto_rotate: bool = True
    mode: Literal['pad', 'crop', 'scale'] = 'pad'
    # size_cm: Size | None = None
    # ppi: float | None = None
    #
    # @pydantic.model_validator(mode='after')
    # def validate_size_cm_ppi_together(self) -> Self:
    #     if (self.ppi is None) != (self.size_cm is None):
    #         raise ValueError('size_cm and ppi must come together')
    #     return self

    def __call__(self, img: np.ndarray, size: Size) -> np.ndarray:
        target_ratio = size.ratio
        ratio = Size.of_image(img).ratio

        if self.auto_rotate and abs(target_ratio - 1 / ratio) < abs(target_ratio - ratio):
            img = RotateOp()(img, size)
            ratio = Size.of_image(img).ratio

        if not math.isclose(ratio, target_ratio, rel_tol=1e-2):
            raise ValueError('Ratios not similar')

        if ratio == target_ratio:
            new_size = size
        elif self.mode == 'crop':
            # if ratio * target.width > target.height -> use height for reference
            new_size = Size(
                round(size.height * ratio),
                size.width
            ) if size.width * ratio > size.width else (
                size.width,
                round(size.width / ratio)
            )
        elif self.mode == 'pad':
            new_size = Size(
                round(size.height * ratio),
                size.height
            ) if size.height * ratio < size.width else (
                size.width,
                round(size.width / ratio)
            )
        else:
            raise ValueError("Invalid zoom method provided")
        assert new_size <= size, f'{new_size} > {size}'

        result = cv2.resize(img, new_size, interpolation=cv2.INTER_LANCZOS4)
        gray_image = np.full((size.height, size.width, 3), .18 * ((1 << 16) - 1), dtype=np.uint16)
        gray_image[:result.shape[0], :result.shape[1]] = result
        return gray_image


class RotateOp(Operator):
    def __call__(self, img: np.ndarray, size: Size) -> np.ndarray:
        return cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
