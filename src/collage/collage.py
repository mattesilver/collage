import logging
from pathlib import Path
from typing import Annotated, Self, Sequence
import functools as ft

import cv2
import numpy as np
import pydantic

from .base import Operator

from . import op
from .types import Box, Size

logger = logging.getLogger(__name__)


class OperationSelection(pydantic.BaseModel):
    crop: op.CropOp | None = None
    resize: op.ResizeOp | None = None
    rotate: op.RotateOp | None = None
    _op: Annotated[Operator | None, pydantic.Field(init=False)] = None

    @pydantic.model_validator(mode='after')
    def check_card_number_omitted(self) -> Self:
        if len(self.model_fields_set) != 1:
            raise ValueError('Exactly one property allowed')

        op_name = next(iter(self.model_fields_set))
        if getattr(self, op_name) is None:
            match op_name:
                case 'crop':
                    self.crop = op.CropOp()
                case 'resize':
                    self.resize = op.ResizeOp()
                case 'rotate':
                    self.rotate = op.RotateOp()
                case _:
                    raise ValueError(op_name)

        self._op = getattr(self, op_name)

        return self

    def __call__(self, source: np.ndarray, size: Size) -> np.ndarray:
        return self._op(source, size)

    def __str__(self):
        return next(iter(self.model_fields_set - {'_op'}))


def _is_set_none(obj: pydantic.BaseModel, name: str) -> bool:
    return name in obj.model_fields_set and getattr(obj, name) is None


class Fragment(pydantic.BaseModel):
    input: str
    ops: Sequence[OperationSelection]

    def __call__(self, root_dir: Path, target: np.ndarray, box: Box) -> None:
        try:
            source = read_cached(root_dir / self.input)
            assert source is not None, 'File not found'
            logger.info('raw input [%s] size %s', self.input, Size.of_image(source))
            for op in self.ops:
                logger.info('%s: %s', self.input, op)
                source = op(source, box.size)
                logger.debug('Size after: %s', Size.of_image(source))
                assert source is not None, f'Op {op} resulted in None'

            size = Size.of_image(source)
            if size != box.size:
                raise ValueError(f'{self.input} shape mismatch')

            target[
            box.y:box.y + box.height,
            box.x:box.x + box.width,
            ] = source
        except (Exception, AssertionError) as e:
            e.add_note(f'Source: {self.input}')
            raise


@ft.cache
def read_cached(path: Path) -> np.ndarray:
    return cv2.imread(str(path), cv2.IMREAD_UNCHANGED)


class Collage(pydantic.RootModel):
    root: list['Fragment | Collage']

    def __call__(self, root_dir: Path, target: np.ndarray, box: Box) -> None:
        width, height = box.size
        if width >= height:
            size = Size(width // len(self.root), height)
            shift = Size(size.width, 0)
        else:
            size = Size(width, height // len(self.root))
            shift = Size(0, size.height)

        sub_box = Box.combine(box.shift, size)
        for elem in self.root:
            elem(root_dir, target, sub_box)
            sub_box += shift


class TargetFile(pydantic.BaseModel):
    size_px: Size
    ppi: float | None = None

    collage: Collage

    def __call__(self, root_dir: Path, output_path: Path) -> None:
        box = Box(0, 0, int(self.size_px.width), int(self.size_px.height))
        target_image = np.zeros((box.size.height, box.size.width, 3), dtype=np.uint16)
        self.collage(root_dir, target_image, box)
        cv2.imwrite(str(root_dir / output_path), target_image)


class Script(pydantic.RootModel):
    root: dict[str, TargetFile]

    def __call__(self, root_dir: Path) -> None:
        for path, target in self.root.items():
            try:
                target(root_dir, Path(path))
            except (Exception, AssertionError) as e:
                e.add_note(f'Target: {path}')
                raise
