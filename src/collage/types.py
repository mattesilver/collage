from typing import Any, NamedTuple, Self

import numpy as np


class Point(NamedTuple):
    x: int
    y: int

    def __add__(self, other: Any) -> Self:
        if not isinstance(other, Size):
            return NotImplemented
        return Point(self.x + other.width, self.y + other.height)

    def __repr__(self) -> str:
        return f"({self.x}, {self.y})"


class Size(NamedTuple):
    width: int
    height: int

    def __repr__(self) -> str:
        return f"{self.width}x{self.height}"

    @classmethod
    def of_image(cls, image: np.ndarray) -> Self:
        return cls(image.shape[1], image.shape[0])

    @property
    def ratio(self) -> float:
        return self.width / self.height

    def __le__(self: Self, other: Any) -> bool:
        if not isinstance(other, Size):
            return NotImplemented
        return self.width <= other.width and self.height <= other.height


class Box(NamedTuple):
    x: int
    y: int
    width: int
    height: int

    @property
    def size(self) -> Size:
        return Size(self.width, self.height)

    @property
    def shift(self) -> Point:
        return Point(self.x, self.y)

    def __repr__(self) -> str:
        return f"({self.width}x{self.height})@({self.x}, {self.y})"

    def __add__(self, other: Any) -> Self:
        if isinstance(other, Size):
            return Box(self.x + other.width, self.y + other.height, self.width, self.height)
        return NotImplemented

    @classmethod
    def combine(cls, point: Point, size: Size) -> "Box":
        return cls(*point, *size)
