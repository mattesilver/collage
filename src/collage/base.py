import abc
import logging

import numpy as np
import pydantic

from .types import Size

logger = logging.getLogger(__name__)


class Operator(abc.ABC, pydantic.BaseModel):
    @abc.abstractmethod
    def __call__(self, img: np.ndarray, size: Size) -> np.ndarray:
        pass

    def __str__(self)->str:
        return type(self).__name__
