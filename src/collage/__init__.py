import numpy as np

from .types import Box


def fit_image(image: np.ndarray, box: Box, gray_level=0.18):
    """
    Take an image and paste it to the target image at position box.x, box.y (x,y).
    If the image is larger than box.width, box.height along any dimension, it will be cropped.
    If it's smaller, the right or bottom area will be filled with gray. Works on 16-bit RGB.

    Args:
        image (np.ndarray): The image to be inserted.
        target (np.ndarray): The target image where the image will be inserted.
        box (Box): The box defining the position and size (x, y, width, height) where the image will be inserted.
        gray_level (float): The gray level to fill the empty areas if the image is smaller. Default is 0.18.
    """

    # Ensure the gray level is scaled correctly for 16-bit images
    gray_value = int(gray_level * 65535)

    # Determine the dimensions to use for cropping and pasting
    crop_height = min(image.shape[0], box.height)
    crop_width = min(image.shape[1], box.width)

    # Crop the image to fit within the box dimensions
    cropped_image = image[:crop_height, :crop_width]

    # Create a gray-filled image of the size of the box
    gray_image = np.full((box.height, box.width, 3), gray_value, dtype=np.uint16)

    # Paste the cropped image into the gray image
    gray_image[:crop_height, :crop_width] = cropped_image

    return gray_image


###
